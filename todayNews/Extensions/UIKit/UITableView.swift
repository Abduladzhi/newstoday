//
//  UITableView.swift
//  todayNews
//
//  Created by Abduladzhi on 11.06.2023.
//

import UIKit

extension UICollectionView {
    func register(cellTypes: [UICollectionViewCell.Type]) {

        cellTypes.forEach {
            let reuseIdentifier = $0.className
            register($0, forCellWithReuseIdentifier: reuseIdentifier)
        }
    }
}

extension UITableView {
    func register(cellTypes: [UITableViewCell.Type]) {

        cellTypes.forEach {
            let reuseIdentifier = $0.className
            register($0, forCellReuseIdentifier: reuseIdentifier)
        }
    }
}

class UITableViewCellWithIdentifire: UITableViewCell {
    static var nib: UINib {
        return UINib(nibName: identifier, bundle: nil)
    }

    static var identifier: String {
        return String(describing: self)
    }
}
