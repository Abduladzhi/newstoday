//
//  UIColor.swift
//  todayNews
//
//  Created by Abduladzhi on 11.06.2023.
//

import UIKit

extension UIColor {
    class var appWhite: UIColor {
        return UIColor(named: "appWhite")!
    }
    class var appBlack: UIColor {
        return UIColor(named: "appBlack")!
    }
}
