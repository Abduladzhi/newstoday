//
//  Locale+acceptLanguage.swift
//  todayNews
//
//  Created by Abduladzhi on 11.06.2023.
//

import Foundation

public extension Locale {
    var acceptLanguage: String {
        identifier.replacingOccurrences(of: "_", with: "-")
    }
}
