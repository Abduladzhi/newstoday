//
//  NSObject+ClassName.swift
//  todayNews
//
//  Created by Abduladzhi on 11.06.2023.
//

import Foundation

extension NSObject {
  static var className: String {
    return String(describing: self)
  }
  var className: String {
    return String(describing: type(of: self))
  }
}
