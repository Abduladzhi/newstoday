//
//  EndPoint.swift
//  todayNews
//
//  Created by Abduladzhi on 11.06.2023.
//

import Foundation


extension Endpoint {
    var url: URL? {
        let url = API.baseURL
        return URL(string: url + path)
    }
}
