//
//  Router.swift
//  todayNews
//
//  Created by Abduladzhi on 11.06.2023.
//

import UIKit

extension Router {

    // MARK: - openFirstScreen

    func openTabBarViewController() {
        navigationController.viewControllers = [TabBarViewController()]
    }

    // MARK: - openDetailScreen

    func openDetailScreen(news: News) {
        let dataService = NewsService()
        let model = DetailNewsModel(newsDataService: dataService)
        let controller = DetailNewsViewController(model: model, news: news)
        navigationController.pushViewController(controller, animated: true)
    }
}

final class Router {

    static let shared = Router()

    private init() {}

    // MARK: - Properties

    let navigationController = UINavigationController()


    // MARK: - PopToRoot

    func popToRoot() {
        navigationController.popToRootViewController(animated: true)
    }

    // MARK: - PopToViewController

    func popToViewController(on view: UIViewController) {
        navigationController.popToViewController(view, animated: true)
    }

    // MARK: - Pop

    func pop(animated: Bool) {
        navigationController.popViewController(animated: animated)
    }

    // MARK: - Dismiss

    func dismiss() {
        navigationController.dismiss(animated: true, completion: nil)
    }
}

