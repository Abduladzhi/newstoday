//
//  TabBarController.swift
//  todayNews
//
//  Created by Abduladzhi on 11.06.2023.
//

import Foundation
import UIKit

final class TabBarViewController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()
        let newsModel = NewsModel(newsDataService: NewsDataService())
        let news = NewsViewController(model: newsModel)

        let favoriteModel = FavoritesModel(newsDataService: NewsService())
        let favorite = FavoritesViewController(model: favoriteModel)

        viewControllers = [news, favorite]

        viewControllers?[0].tabBarItem = UITabBarItem(title: "Новости", image: UIImage(systemName: "newspaper"),
                                                      selectedImage: nil)
        viewControllers?[1].tabBarItem = UITabBarItem(title: "Избранное", image: UIImage(systemName: "heart"),
                                                      selectedImage: nil)
    }
}
