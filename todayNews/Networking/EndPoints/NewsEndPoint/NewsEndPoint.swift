//
//  NewsEndPoint.swift
//  todayNews
//
//  Created by Abduladzhi on 11.06.2023.
//

import Foundation

enum NewsServiceEndpoint: Endpoint {
    case fetchNews
    case fetchNewsNextPage(page: String)

    var path: String {
        switch self {
        case .fetchNews, .fetchNewsNextPage:
            return API.News.news
        }
    }

    var requestType: RequestType {
        switch self {
        case .fetchNews, .fetchNewsNextPage:
            return .get
        }
    }

    var header: [String : String]? {
        return ["Content-Type": "application/json", "Accept": "application/json"]
    }

    var parameters: [String : Any]? {
        switch self {
        case .fetchNews:
            return ["apikey": "pub_24348a0bd188ebf95f5d22caf1c0ff46bff83",
                    "language": "ru"]
        case .fetchNewsNextPage(let page):
            return ["apikey": "pub_24348a0bd188ebf95f5d22caf1c0ff46bff83",
                    "language": "ru",
                    "page": page]
        }
    }
}
