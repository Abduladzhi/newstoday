//
//  API.swift
//  todayNews
//
//  Created by Abduladzhi on 11.06.2023.
//

struct API {
    static let baseURL = "https://newsdata.io/api/1"
    struct News {
        static let news = "/news"
    }
}
