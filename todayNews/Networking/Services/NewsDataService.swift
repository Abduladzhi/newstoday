//
//  NewsDataService.swift
//  todayNews
//
//  Created by Abduladzhi on 11.06.2023.
//

protocol NewsDataServiceProtocol {
    func fetchNews() async throws -> NewsData
    func fetchNewsNextPage(page: String) async throws -> NewsData
}

actor NewsDataService: NewsDataServiceProtocol {
    static let newsDataService = NewsDataService()

    func fetchNews() async throws -> NewsData {
        guard let result = await APIManager().sendRequest(
            model: NewsData.self,
            endpoint: NewsServiceEndpoint.fetchNews
        ) else {
            throw RequestError.statusNotOk
        }

        switch result {
        case .success(let response):
            return response
        case .failure:
            throw RequestError.statusNotOk
        }
    }
    func fetchNewsNextPage(page: String) async throws -> NewsData {
        guard let result = await APIManager().sendRequest(
            model: NewsData.self,
            endpoint: NewsServiceEndpoint.fetchNewsNextPage(page: page)
        ) else {
            throw RequestError.statusNotOk
        }

        switch result {
        case .success(let response):
            return response
        case .failure:
            throw RequestError.statusNotOk
        }
    }
}
