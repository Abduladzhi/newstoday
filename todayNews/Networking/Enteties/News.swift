//
//  News.swift
//  todayNews
//
//  Created by Abduladzhi on 11.06.2023.
//

struct NewsData: Codable {
    let status: String?
    let totalResults: Int?
    let results: [News]?
    let nextPage: String?
}

// MARK: - News

struct News: Codable {
    let title: String?
    let link: String?
    let keywords: [String]?
    let creator: [String]?
    let description, content, pubDate: String?
    let imageURL: String?

    enum CodingKeys: String, CodingKey {
        case title, link, keywords, creator, description, content, pubDate
        case imageURL = "image_url"
    }
}
