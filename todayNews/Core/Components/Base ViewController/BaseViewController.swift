//
//  BaseViewController.swift
//  todayNews
//
//  Created by Abduladzhi on 11.06.2023.
//

import UIKit

class BaseViewController: UIViewController, AlertPresentable {

    //MARK: - Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
        setupInterface()
    }

    //MARK: - SetupInterface

    private func setupInterface() {
        view.backgroundColor = .white
    }
}
