//
//  NewsTableView.swift
//  todayNews
//
//  Created by Abduladzhi on 11.06.2023.
//

import Foundation
import UIKit

protocol NewsTableViewDelegate: AnyObject {
    func fetchNextPage()
}

final class NewsTableView: UITableView, UITableViewDelegate, UITableViewDataSource {

    //MARK: Delegate

    weak var newsTableViewDelegate: NewsTableViewDelegate?

    //MARK: - Properties

    private var isLoading = false
    var news: [News] = [] {
        didSet {
            reloadData()
            isLoading = false
        }
    }
    var isFavorite = false

    // MARK: - LifeCycle

    override func didMoveToSuperview() {
        super.didMoveToSuperview()
        setupView()
        registerTableViewCell()
        setupInterface()
    }

    // MARK: - Setups

    private func setupView() {
        delegate = self
        dataSource = self
    }

    private func registerTableViewCell() {
        register(cellTypes: [NewsCell.self, LoadingCell.self])
    }

    private func setupInterface() {
        backgroundColor = .white
        separatorStyle = .none
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return news.count
        } else {
            return 1
        }
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: NewsCell.className, for: indexPath) as! NewsCell
            cell.setupContent(news: news[indexPath.row])
            cell.selectionStyle = .none
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: LoadingCell.className, for: indexPath) as! LoadingCell
            cell.selectionStyle = .none
            if isLoading {
                cell.indicatorView.startAnimating()
            } else {
                cell.indicatorView.stopAnimating()
            }
            if isFavorite {
                cell.indicatorView.isHidden = true
            }
            return cell
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 0 {
            Router.shared.openDetailScreen(news: news[indexPath.row])
        }
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row == news.count - 2, !isLoading {
            isLoading = true
            newsTableViewDelegate?.fetchNextPage()
        }
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
}
