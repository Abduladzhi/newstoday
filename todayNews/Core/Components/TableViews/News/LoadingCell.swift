//
//  LoadingCell.swift
//  todayNews
//
//  Created by Abduladzhi on 11.06.2023.
//

import Foundation
import UIKit

final class LoadingCell: UITableViewCell {

    //MARK: - Views

    var indicatorView: UIActivityIndicatorView = {
        let indicatorView = UIActivityIndicatorView(style: .medium)
        indicatorView.color = .appBlack
        return indicatorView
    }()

    //MARK: - DidMoveToSuperview

    override func didMoveToSuperview() {
        super.didMoveToSuperview()
        setupViews()
        setupConstraints()
    }

    //MARK: - SetupViews

    private func setupViews() {
        contentView.addSubview(indicatorView)
    }

    //MARK: - SetupConstraints

    private func setupConstraints() {
        indicatorView.snp.makeConstraints {
            $0.centerX.equalToSuperview()
            $0.size.equalTo(20)
            $0.top.bottom.equalToSuperview().inset(10)
        }
    }
}
