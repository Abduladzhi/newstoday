//
//  NewsCell.swift
//  todayNews
//
//  Created by Abduladzhi on 11.06.2023.
//

import Foundation
import UIKit

final class NewsCell: UITableViewCell {

    //MARK: - Views

    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.font = .systemFont(ofSize: 12, weight: .medium)
        label.numberOfLines = 0
        contentView.addSubview(label)
        return label
    }()
    private lazy var dateLabel: UILabel = {
        let label = UILabel()
        label.font = .systemFont(ofSize: 12, weight: .medium)
        contentView.addSubview(label)
        return label
    }()
    private lazy var authorLabel: UILabel = {
        let label = UILabel()
        label.font = .systemFont(ofSize: 12, weight: .medium)
        contentView.addSubview(label)
        return label
    }()
    private lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label.font = .systemFont(ofSize: 12, weight: .medium)
        label.numberOfLines = 0
        contentView.addSubview(label)
        return label
    }()
    private lazy var previewImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        contentView.addSubview(imageView)
        return imageView
    }()
    private lazy var separatorLineView: UIView = {
        let view = UIView()
        view.backgroundColor = .appBlack
        contentView.addSubview(view)
        return view
    }()

    //MARK: - DidMoveToSuperview

    override func didMoveToSuperview() {
        super.didMoveToSuperview()
        setupConstraints()
    }

    //MARK: - SetupConstraints

    private func setupConstraints() {
        titleLabel.snp.makeConstraints {
            $0.top.equalToSuperview().offset(8)
            $0.leading.equalToSuperview().inset(16)
            $0.trailing.equalTo(previewImageView.snp.leading).offset(-4)
        }
        descriptionLabel.snp.makeConstraints {
            $0.top.equalTo(titleLabel.snp.bottom).offset(4)
            $0.leading.equalToSuperview().inset(16)
            $0.trailing.equalTo(previewImageView.snp.leading).offset(-4)
        }
        authorLabel.snp.makeConstraints {
            $0.leading.equalToSuperview().offset(16)
            $0.top.equalTo(descriptionLabel.snp.bottom).offset(4)
        }
        dateLabel.snp.makeConstraints {
            $0.leading.equalTo(authorLabel.snp.trailing).offset(4)
            $0.top.equalTo(descriptionLabel.snp.bottom).offset(4)
        }
        separatorLineView.snp.makeConstraints {
            $0.height.equalTo(1)
            $0.top.equalTo(dateLabel.snp.bottom).offset(8)
            $0.leading.trailing.equalToSuperview().inset(16)
            $0.bottom.equalToSuperview()
        }
        previewImageView.snp.makeConstraints {
            $0.size.equalTo(40)
            $0.trailing.equalToSuperview().offset(-16)
            $0.top.equalToSuperview().offset(8)
        }
    }

    //MARK: - SetupContent

    func setupContent(news: News) {
        titleLabel.text = news.title
        descriptionLabel.text = news.description
        dateLabel.text = news.pubDate
        if let imageUrl = news.imageURL {
            previewImageView.sd_setImage(with: URL(string: imageUrl))
            previewImageView.snp.updateConstraints {
                $0.size.equalTo(40)
            }
        } else {
            previewImageView.snp.updateConstraints {
                $0.size.equalTo(0)
            }
        }
        guard let author = news.creator?.first else {
            authorLabel.text = "Автор неизвестен"
            return
        }
        authorLabel.text = "Автор: \(author)"
    }
}
