//
//  DetailNewsViewController.swift
//  todayNews
//
//  Created by Abduladzhi on 11.06.2023.
//

import Foundation
import UIKit
import SDWebImage

final class DetailNewsViewController: BaseViewController {

    //MARK: - Properties

    private let model: DetailNewsModel
    private let news: News
    private var articleUrl: String?
    private var newsBase: [NewsBaseModel] = []
    private var isFavorite = false

    //MARK: - Views

    private lazy var scrollView: UIScrollView = {
        let scrollView = UIScrollView()
        view.addSubview(scrollView)
        return scrollView
    }()
    private lazy var contentView: UIView = {
        let contentView = UIView()
        scrollView.addSubview(contentView)
        return contentView
    }()
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.font = .systemFont(ofSize: 12, weight: .medium)
        label.numberOfLines = 0
        contentView.addSubview(label)
        return label
    }()
    private lazy var dateLabel: UILabel = {
        let label = UILabel()
        label.font = .systemFont(ofSize: 12, weight: .medium)
        contentView.addSubview(label)
        return label
    }()
    private lazy var authorLabel: UILabel = {
        let label = UILabel()
        label.font = .systemFont(ofSize: 12, weight: .medium)
        contentView.addSubview(label)
        return label
    }()
    private lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label.font = .systemFont(ofSize: 12, weight: .medium)
        label.numberOfLines = 0
        contentView.addSubview(label)
        return label
    }()
    private lazy var previewImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.backgroundColor = .gray
        contentView.addSubview(imageView)
        return imageView
    }()
    private lazy var linkButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("Открыть статью", for: .normal)
        button.addTarget(self, action: #selector(openLinkAction), for: .touchUpInside)
        contentView.addSubview(button)
        return button
    }()

    //MARK: - Init

    required init(model: DetailNewsModel, news: News) {
        self.model = model
        self.news = news
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    //MARK: - Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
        fetchNewsInDataBase()
        setupConstraints()
        setupContent()
    }

    //MARK: - SetupConstraints

    private func setupConstraints() {
        scrollView.snp.makeConstraints {
            $0.centerX.width.equalToSuperview()
            $0.top.equalToSuperview()
            $0.bottom.equalToSuperview()
        }
        contentView.snp.makeConstraints {
            $0.centerX.width.top.bottom.equalToSuperview()
        }
        previewImageView.snp.makeConstraints {
            $0.top.equalToSuperview().offset(0)
            $0.size.equalTo(0)
            $0.centerX.equalToSuperview()
        }
        titleLabel.snp.makeConstraints {
            $0.top.equalTo(previewImageView.snp.bottom).offset(12)
            $0.leading.trailing.equalToSuperview().inset(16)
        }
        descriptionLabel.snp.makeConstraints {
            $0.top.equalTo(titleLabel.snp.bottom).offset(12)
            $0.leading.trailing.equalToSuperview().inset(16)
        }
        authorLabel.snp.makeConstraints {
            $0.leading.equalToSuperview().offset(16)
            $0.top.equalTo(descriptionLabel.snp.bottom).offset(4)
        }
        dateLabel.snp.makeConstraints {
            $0.leading.equalTo(authorLabel.snp.trailing).offset(4)
            $0.top.equalTo(descriptionLabel.snp.bottom).offset(4)
        }
        linkButton.snp.makeConstraints {
            $0.top.equalTo(dateLabel.snp.bottom)
            $0.leading.trailing.equalToSuperview().inset(16)
            $0.height.equalTo(55)
            $0.bottom.equalToSuperview().offset(-8)
        }
    }

    //MARK: - FetchNewsInDataBase

    private func fetchNewsInDataBase() {
        let result = model.readNews()
        newsBase = result
    }

    //MARK: - SetupContent

    private func setupContent() {
        if !newsBase.isEmpty {
            if filterNews(newsBase: newsBase, news: news) {
                navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(systemName: "heart.fill"), style: .done, target: self, action: #selector(updateNewsAction))
                isFavorite = true
            } else {
                navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(systemName: "heart"), style: .done, target: self, action: #selector(updateNewsAction))
                isFavorite = false
            }
        } else {
            navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(systemName: "heart"), style: .done, target: self, action: #selector(updateNewsAction))
            isFavorite = false
        }
        titleLabel.text = news.title
        descriptionLabel.text = news.content
        dateLabel.text = news.pubDate
        articleUrl = news.link
        if let imageUrl = news.imageURL {
            previewImageView.snp.updateConstraints {
                $0.top.equalToSuperview().offset(16)
                $0.size.equalTo(Constants.screenSize.width / 2)
                $0.centerX.equalToSuperview()
            }
            previewImageView.sd_setImage(with: URL(string: imageUrl))
        }

        guard let author = news.creator?.first else {
            authorLabel.text = "Автор неизвестен"
            return
        }
        authorLabel.text = "Автор: \(author)"
    }

    //MARK: - Private methods

    @objc private func openLinkAction() {
        guard let url = URL(string: articleUrl ?? "") else {
            return
        }

        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        } else {
            UIApplication.shared.openURL(url)
        }
    }
    func filterNews(newsBase: [NewsBaseModel], news: News) -> Bool {
        for item in newsBase {
            if item.title == news.title {
                return true
            }
        }
        return false
    }
    @objc private func updateNewsAction() {
        if isFavorite {
            model.deleteNews(object: NewsBaseModel(title: news.title ?? "a", link: news.link, keywords: news.keywords?.first, creator: news.creator?.first, descriptions: news.description, content: news.content, pubDate: news.pubDate, imageURL: news.imageURL))
            navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(systemName: "heart"), style: .done, target: self, action: #selector(updateNewsAction))
            isFavorite = false
        } else {
            model.saveNews(object: NewsBaseModel(title: news.title ?? "a", link: news.link, keywords: news.keywords?.first, creator: news.creator?.first, descriptions: news.description, content: news.content, pubDate: news.pubDate, imageURL: news.imageURL))
            navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(systemName: "heart.fill"), style: .done, target: self, action: #selector(updateNewsAction))
            isFavorite = true
        }
    }
}
