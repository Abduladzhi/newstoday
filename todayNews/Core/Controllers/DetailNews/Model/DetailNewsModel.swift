//
//  DetailNewsModel.swift
//  todayNews
//
//  Created by Abduladzhi on 11.06.2023.
//

import Foundation

final class DetailNewsModel {
    private let newsDataService: NewsServiceProtocol

    private var dataBase: [NewsBaseModel] = []

    //MARK: Init

    init (newsDataService: NewsServiceProtocol) {
        self.newsDataService = newsDataService
    }

    func readNews() -> [NewsBaseModel] {
        newsDataService.read(NewsBaseModel.self) { [weak self] newsList in
            self?.dataBase = newsList
        }
        return dataBase
    }
    func saveNews(object: NewsBaseModel) {
        newsDataService.save(object: object) { isSuccess in
            if isSuccess {
                print("Успех")
            } else {
                print("Ошибка")
            }
        }
    }
    func deleteNews(object: NewsBaseModel) {
        newsDataService.delete(object: object) { isSuccess in
            if isSuccess {
                print("Успех")
            } else {
                print("Ошибка")
            }
        }
    }
}
