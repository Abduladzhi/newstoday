//
//  FavoritesModel.swift
//  todayNews
//
//  Created by Abduladzhi on 12.06.2023.
//

import Foundation

final class FavoritesModel {
    private let newsDataService: NewsServiceProtocol

    private var dataBase: [NewsBaseModel] = []

    //MARK: Init

    init (newsDataService: NewsServiceProtocol) {
        self.newsDataService = newsDataService
    }
    func readNews() -> [NewsBaseModel] {
        newsDataService.read(NewsBaseModel.self) { [weak self] newsList in
            self?.dataBase = newsList
        }
        return dataBase
    }
}
