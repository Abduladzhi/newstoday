//
//  FavoritesController.swift
//  todayNews
//
//  Created by Abduladzhi on 12.06.2023.
//

import Foundation
import UIKit

final class FavoritesViewController: BaseViewController {

    //MARK: - Properties

    private let model: FavoritesModel

    //MARK: - Views

    private lazy var newsTableView: NewsTableView = {
        let tableView = NewsTableView()
        tableView.isFavorite = true
        view.addSubview(tableView)
        return tableView
    }()
    //MARK: - Init

    required init(model: FavoritesModel) {
        self.model = model
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    //MARK: - Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
        setupConstraints()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        fetchNews()
    }

    //MARK: - SetupConstraints

    private func setupConstraints() {
        newsTableView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
    }

    //MARK: - FetchNews

    private func fetchNews() {
        var arrayNews: [News] = []
        let result = model.readNews()
        result.forEach { item in
            arrayNews.append(News(title: item.title, link: item.link, keywords: ["\(item.keywords ?? "a")"], creator: ["\(item.creator ?? "a")"], description: item.descriptions, content: item.content, pubDate: item.pubDate, imageURL: item.imageURL))
        }
        newsTableView.news = arrayNews
    }
}
