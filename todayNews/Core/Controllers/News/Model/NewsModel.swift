//
//  NewsModel.swift
//  todayNews
//
//  Created by Abduladzhi on 11.06.2023.
//

import Foundation

final class NewsModel {

    private let newsDataService: NewsDataServiceProtocol

    //MARK: Init

    init (newsDataService: NewsDataServiceProtocol) {
        self.newsDataService = newsDataService
    }

    @MainActor func fetchNews() async throws -> NewsData? {
        do {
            let result = try await newsDataService.fetchNews()
            return result
        } catch {
            print(#function, error.localizedDescription)
            return nil
        }
    }
    @MainActor func fetchNewsNextPage(page: String) async throws -> NewsData? {
        do {
            let result = try await newsDataService.fetchNewsNextPage(page: page)
            return result
        } catch {
            print(#function, error.localizedDescription)
            return nil
        }
    }
}
