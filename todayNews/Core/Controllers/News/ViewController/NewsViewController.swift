//
//  NewsViewController.swift
//  todayNews
//
//  Created by Abduladzhi on 11.06.2023.
//

import Foundation
import UIKit

final class NewsViewController: BaseViewController {

    //MARK: - Properties

    private let model: NewsModel
    private var pageID: String?

    //MARK: - Views

    private lazy var newsTableView: NewsTableView = {
        let tableView = NewsTableView()
        tableView.newsTableViewDelegate = self
        view.addSubview(tableView)
        return tableView
    }()
    private lazy var indicatorView: UIActivityIndicatorView = {
        let indicatorView = UIActivityIndicatorView(style: .medium)
        indicatorView.color = .appBlack
        indicatorView.startAnimating()
        view.addSubview(indicatorView)
        return indicatorView
    }()


    //MARK: - Init

    required init(model: NewsModel) {
        self.model = model
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    //MARK: - Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
        fetchNews()
        setupConstraints()
    }

    //MARK: - SetupConstraints

    private func setupConstraints() {
        newsTableView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
        indicatorView.snp.makeConstraints {
            $0.centerX.centerY.equalToSuperview()
        }
    }

    //MARK: - FetchNews

    private func fetchNews() {
        Task {
            guard let result = try await model.fetchNews() else {
                presentAlert()
                return
            }
            indicatorView.stopAnimating()
            newsTableView.news = result.results ?? []
            pageID = result.nextPage
        }
    }
    private func presentAlert() {
        showAlert(title: "Проблема с интернетом", message: nil, actions: [.init(title: "Повторить", style: .default, action: { [weak self] in
            self?.fetchNews()
        })], style: .alert)
    }
}

//MARK: - extension NewsTableViewDelegate

extension NewsViewController: NewsTableViewDelegate {
    func fetchNextPage() {
        Task {
            guard let page = pageID else { return }
            guard let result = try await model.fetchNewsNextPage(page: page) else { return }
            newsTableView.news.append(contentsOf: result.results ?? [])
            pageID = result.nextPage
        }
    }
}
