//
//  RequestError.swift
//  todayNews
//
//  Created by Abduladzhi on 11.06.2023.
//

import Foundation

public enum RequestError: Error {
    case statusNotOk
    case decodingError(DecodingError)
    case invalidURL
    case noResponse
    case unexpectedStatusCode
    case unknown(Data)

    var statusCode: Int {
        switch self {
        case .noResponse:
            return 400
        default:
            return 403
        }
    }
}

extension DecodingError {
    var message: String {
        var output = ""

        switch self {
        case .dataCorrupted(let context):
            output = context.debugDescription
        case .keyNotFound(let key, let context):
            output += String(format: "Key '\(key)' not found: %@", context.debugDescription)
            output += String(format: "codingPath: %@", context.codingPath)
        case .valueNotFound(let value, let context):
            output += String(format: "Value '\(value)' not found: %@", context.debugDescription)
            output += String(format: "codingPath: %@", context.codingPath)
        case .typeMismatch(let type, let context):
            output += String(format: "Type '\(type)' mismatch: %@", context.debugDescription)
            output += String(format: "codingPath: %@", context.codingPath)
        default:
            output = localizedDescription
        }

        return output
    }
}
