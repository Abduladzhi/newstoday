//
//  UploadData.swift
//  todayNews
//
//  Created by Abduladzhi on 11.06.2023.
//

import Foundation

public struct UploadData {
    let mimetype: String?
    let file: Data
}
