//
//  RequestType.swift
//  todayNews
//
//  Created by Abduladzhi on 11.06.2023.
//

import Foundation

public enum RequestType: String {
    case post = "POST"
    case get = "GET"
    case delete = "DELETE"
    case patch = "PATCH"
    case put = "PUT"
}
