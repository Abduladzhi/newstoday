//
//  Endpoint.swift
//  todayNews
//
//  Created by Abduladzhi on 11.06.2023.
//

import Foundation

public protocol Endpoint {
    var path: String { get }
    var requestType: RequestType { get }
    var header: [String: String]? { get }
    var parameters: [String: Any]? { get }
    var url: URL? { get }
}
