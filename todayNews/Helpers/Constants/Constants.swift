//
//  Constants.swift
//  todayNews
//
//  Created by Abduladzhi on 11.06.2023.
//

import UIKit

final class Constants {
    static let isThiniPhone = UIScreen.main.bounds.width < 375
    static let isSmalliPhone = UIScreen.main.bounds.height < 750
    static let screenSize = UIScreen.main.bounds.size
    static let placeholderImage = UIImage(named: "no-image")
}
