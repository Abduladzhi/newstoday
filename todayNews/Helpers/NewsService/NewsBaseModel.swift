//
//  NewsBaseModel.swift
//  todayNews
//
//  Created by Abduladzhi on 12.06.2023.
//

import Foundation
import RealmSwift
import UIKit

class NewsBaseModel: Object {
    @Persisted var title: String
    @Persisted var link: String?
    @Persisted var keywords: String?
    @Persisted var creator: String?
    @Persisted var descriptions: String?
    @Persisted var content: String?
    @Persisted var pubDate: String?
    @Persisted var imageURL: String?

    convenience init(title: String, link: String? = nil, keywords: String? = nil, creator: String? = nil, descriptions: String? = nil, content: String? = nil, pubDate: String? = nil, imageURL: String? = nil) {
        self.init()
        self.title = title
        self.link = link
        self.keywords = keywords
        self.creator = creator
        self.descriptions = descriptions
        self.content = content
        self.pubDate = pubDate
        self.imageURL = imageURL
    }
}
