//
//  DataBaseService.swift
//  todayNews
//
//  Created by Abduladzhi on 12.06.2023.
//

import Foundation
import RealmSwift

protocol NewsServiceProtocol {
    func read<T: Object>(_: T.Type, completion: @escaping ([T]) -> Void)
    func save(object: Object, completion: @escaping (_ isSuccess: Bool) -> Void)
    func delete(object: NewsBaseModel, completion: @escaping (_ isSuccess: Bool) -> Void)
}

final class NewsService: NewsServiceProtocol {
    private let realm = try! Realm()
    func read<T: Object>(_: T.Type, completion: @escaping ([T]) -> Void) {
        completion(Array(realm.objects(T.self)))
    }

    func save(object: Object, completion: @escaping (_ isSuccess: Bool) -> Void) {
        DispatchQueue.main.async {
            do {
                try self.realm.write {
                    self.realm.add(object)
                    completion(true)
                }
            } catch {
                completion(false)
            }
        }
    }

    func delete(object: NewsBaseModel, completion: @escaping (_ isSuccess: Bool) -> Void) {
        do {
            try realm.write {
                realm.delete(realm.objects(NewsBaseModel.self).filter("title=%@",object.title))
                completion(true)

            }
        } catch {
            completion(false)
        }
    }
}

