//
//  AlertPresentable.swift
//  todayNews
//
//  Created by Abduladzhi on 12.06.2023.
//

import Foundation
import UIKit

protocol AlertPresentable: AnyObject {
    func showAlert(title: String?, message: String?, actions: [AlertAction], style: UIAlertController.Style)
}

extension AlertPresentable where Self: UIViewController {

    func showAlert(title: String?, message: String?, actions: [AlertAction], style: UIAlertController.Style) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: style)
        let alertActions = actions.map { (action) in
            return UIAlertAction(title: action.title, style: action.style, handler: { (_) in
                action.action?()
            })
        }
        alertActions.forEach {
            alert.addAction($0)
        }

        present(alert, animated: true, completion: nil)
    }
}

struct AlertAction {
    let title: String
    let style: UIAlertAction.Style
    let action: (() -> Void)?
}
